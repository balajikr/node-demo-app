'use strict';

const registerRoute = require('./controllers/register');

module.exports = (app) => {
  app.use('/api/register', registerRoute);
};
