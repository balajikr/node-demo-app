'use strict';

const mongoose = require('mongoose');
const { Schema, model } = mongoose;

const registerSchema = new Schema({
  first_name: String,
  last_name: String,
  email: {
    type: String,
    createIndex: true
  },
  password: String,
  dob: String,
});

const registerModel = model('register', registerSchema);

module.exports = registerModel;
