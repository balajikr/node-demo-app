const registerModel = require('../../models/register.model');
const { ObjectId } = require('mongoose').Types;

exports.index = function (req, res) {
  registerModel
    .find()
    .then((users) => res.status(200).send({ users }))
    .catch(() => res.status(500).send('Something went wrong'));
};

exports.show = function (req, res) {
  const { id } = req.params;
  registerModel
    .findById(id)
    .then((user) => res.status(200).send({ user }))
    .catch(() => res.status(500).send('Something went wrong'));
};

exports.create = function (req, res) {
  const { first_name, last_name, dob, email, password } = req.body;
  const newUser = new registerModel({
    first_name,
    last_name,
    dob,
    email,
    password,
  });
  newUser
    .save()
    .then((register) => res.status(201).send({ register }))
    .catch(() => res.status(500).send('Something went wrong'));
};

exports.upsert = function (req, res) {
  const { id } = req.params;
  registerModel
    .findOneAndUpdate(
      {
        _id: ObjectId(id),
      },
      req.body
    )
    .then((user) => res.status(200).send('User Updated Successfully'))
    .catch(() => res.status(500).send('Something went wrong'));
};

exports.destroy = function (req, res) {
  const { id } = req.params;
  registerModel
    .deleteOne({
      _id: new ObjectId(id),
    })
    .then(() => res.status(200).send('User Deleted Successfully'))
    .catch(() => res.status(500).send('Something went wrong'));
};
