'use strict';

const express = require('express');
const mongoose = require('mongoose');
const http = require('http');
const app = express();
const bodyParser = require('body-parser');
const routerConfig = require('./src/routes');

const server = http.createServer(app);

mongoose
  .connect('mongodb://localhost:27017/test-vetri', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log('Mongo DB Connected!!!');
  })
  .catch(() => {
    console.log('DB Connection failure!!!!');
    process.exit(-1);
  });

app.use(express.json());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

routerConfig(app);
app.get('/', (req, res) => res.send('Server Up!!'));
server.listen(3030, () => console.log('Server listening on PORT', 3030));

module.exports = app;
